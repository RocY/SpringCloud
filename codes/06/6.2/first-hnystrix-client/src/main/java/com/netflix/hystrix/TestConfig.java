package com.netflix.hystrix;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixThreadPoolKey;
import com.netflix.hystrix.HystrixThreadPool.Factory;

public class TestConfig {

	public static void main(String[] args) throws Exception {
		RunCommand c = new RunCommand("");
		HystrixThreadPool p = Factory.threadPools.get("abc");
		System.out.println(c.threadPool);
		System.out.println(p);
	}

	// 测试命令
	static class RunCommand extends HystrixCommand<String> {

		String msg;
		
		public RunCommand(String msg) {
			super(
					Setter.withGroupKey(
							HystrixCommandGroupKey.Factory.asKey("group-key"))
							.andCommandKey(HystrixCommandKey.Factory.asKey("command-key"))
							.andThreadPoolKey(HystrixThreadPoolKey.Factory.asKey("pool-key"))
					
					);
			this.msg = msg;
		}

		protected String run() throws Exception {
			System.out.println(msg);
			return "success";
		}
		
		public String getPoolKey() {
			return super.threadPoolKey.name();
		}
	}
}
