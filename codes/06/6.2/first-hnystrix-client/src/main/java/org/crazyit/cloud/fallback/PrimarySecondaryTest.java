package org.crazyit.cloud.fallback;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

public class PrimarySecondaryTest {

	public static void main(String[] args) throws Exception {
		MainCommand c = new MainCommand();
		c.execute();
		Thread.sleep(1000);
	}

	static class MainCommand extends HystrixCommand<String> {
		
		public MainCommand() {
			super(HystrixCommandGroupKey.Factory.asKey("MainCommand"));
		}

		@Override
		protected String run() throws Exception {
			new CommandA().execute();
			new CommandB().execute();
			return "";
		}

		@Override
		protected String getFallback() {
			System.out.println("MainCommand fallback");
			return "";
		}		
	}
	
	static class CommandA extends HystrixCommand<String> {
		
		public CommandA() {
			super(HystrixCommandGroupKey.Factory.asKey("CommandA"));
		}

		@Override
		protected String run() throws Exception {
			System.out.println("CommandA run 方法");
			throw new RuntimeException();
		}
	}
	
	static class CommandB extends HystrixCommand<String> {
		
		public CommandB() {
			super(HystrixCommandGroupKey.Factory.asKey("CommandB"));
		}

		@Override
		protected String run() throws Exception {
			System.out.println("CommandB run 方法");
			return "";
		}
		
	}
}
