package org.crazyit.feign.interceptor;

import org.crazyit.feign.PersonClient;

import feign.Feign;

public class InterceptorTest {

	public static void main(String[] args) {
		// 获取服务接口
		PersonClient personClient = Feign.builder()
				.requestInterceptor(new MyInterceptor())
				.target(PersonClient.class, "http://localhost:8080/");
	}
}
